<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="com.derinaldis.sigea.QueryInfo"%>
<%@page import="com.derinaldis.sigea.QueryBuilder"%>
<!DOCTYPE html>
<jsp:useBean id="info" class="com.derinaldis.sigea.QueryInfo"
	scope="session" />
<html>
<head>
<%@ include file="common/head.html"%>
<link rel="stylesheet"
	href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css" />
</head>
<body>
	<%@ include file="common/body.html"%>
	<div id="form-container">
		<form action="FormController" method="post" class="form-style-9">
			<%
				if (info.hasErrors()) {
					out.print("<section class=\"errors\">");
					out.print("<ul>");
					for (String error : info.getErrors()) {
						out.print("<li>" + error + "</li>");
					}
					out.print("</ul>");
					out.print("</section>");
				}
			%>
			<ul>
				<li>
					<div class="align-left">
						<label for="COMPAGNIA"> COMPAGNIA&nbsp; </label> <select
							name="COMPAGNIA" class="select-style">
							<%
								for (String compagnia : QueryInfo.COMPAGNIE) {
									out.print("<option value=\"" + compagnia + "\"");
									out.print(info.getCOMPAGNIA().equals(compagnia) ? "selected" : "");
									out.print(">" + compagnia + "</option>");
								}
							%>
						</select>
					</div>
				</li>
				<li>
					<div class="align-left">
						<label for="CTIPO_TBUT"> Tipo tabulato&nbsp; </label> <input
							type="text" name="CTIPO_TBUT" placeholder="Tipo tabulato"
							value="<%out.print(info.getCTIPO_TBUT());%>" id="CTIPO_TBUT"
							class="field-style field-full" />
					</div>
				</li>
				<li>
					<div class="align-left">
						<label for="CIDCV_TBUT"> Sotto tabulato&nbsp; </label> <input
							type="text" name="CIDCV_TBUT" placeholder="Sotto tabulato"
							value="<%out.print(info.getCIDCV_TBUT());%>" id="CIDCV_TBUT"
							class="field-style field-full" />
					</div>
				</li>
				<li>
					<div class="align-left">
						<label for="CCIVE_GNCA_2"> Numero polizza&nbsp; </label> <input
							type="text" name="CCIVE_GNCA_2" placeholder="Numero polizza"
							value="<%out.print(info.getCCIVE_GNCA_2());%>" id="CCIVE_GNCA_2"
							class="field-style field-full" />
					</div>
				</li>
				<li>
					<div class="align-left">
						<label for="CCIVE_GNCA_1"> Data di riferimento&nbsp; </label> <input
							type="text" name="CCIVE_GNCA_1" placeholder="Data di riferimento"
							value="<%out.print(info.getCCIVE_GNCA_1());%>" id="CCIVE_GNCA_1"
							class="field-style field-full" />
					</div>
				</li>
				<li>
					<div class="align-left">
						<label for="NPRTT"> Numero partita (facoltativo)&nbsp; </label> <input
							type="text" name="NPRTT"
							placeholder="Numero partita (facoltativo)"
							value="<%out.print(info.getNPRTT());%>" id="NPRTT"
							class="field-style field-full" />
					</div>
				</li>
				<li><input type="submit" name="action_select"
					value="Genera la select" /> <!--  <input type="submit" name="action_insert" value="Genera la insert" 
					class="align-right" /> --></li>
			</ul>
		</form>
		<div class="form-style-9">
			<ul>
				<li>
					<div class="align-left">
						<label for="query">Query&nbsp;</label>
						<textarea name="query" rows="7" , cols="46" , readonly id="query"
							class="field-style"><%if (!info.hasErrors()) {
									out.print(QueryBuilder.getInstance().buildQuery(info));
								}%></textarea>
					</div>
				</li>
				<li><span>&nbsp;</span></li>
				<li><input type="button" onclick="copyText()"
					value="Copia il testo" /></li>
			</ul>
		</div>
	</div>
	<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<script>
		$(function() {
			$.datepicker.setDefaults($.datepicker.regional["it"]);
			$("#CCIVE_GNCA_1").datepicker({
				dateFormat : "dd-mm-yy",
				changeMonth : true,
				changeYear : true,
				showButtonPanel : true,
				maxDate : "+0m"
			});
		});
		function copyText() {
			var copyText = document.getElementById("query");
			copyText.select();
			document.execCommand("copy");
		}
	</script>
</body>
</html>