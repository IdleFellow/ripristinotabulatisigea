package com.derinaldis.sigea;

import java.io.IOException;

public class QueryBuilder {
	private static QueryBuilder instance;

	public static QueryBuilder getInstance() {
		if (instance == null) {
			try {
				instance = new QueryBuilder();
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		}
		return instance;
	}

	QueryBuilderImpl selectBuilder, insertBuilder;

	private QueryBuilder() throws IOException {
		selectBuilder = new SelectBuilder();
		insertBuilder = new InsertBuilder();
	}

	public String buildQuery(QueryInfo info) {
		if (!info.isFilledIn()) {
			return "";
		}

		if (info.getAction() == QueryInfo.Action.SELECT) {
			return selectBuilder.buildQuery(info);
		} else if (info.getAction() == QueryInfo.Action.INSERT) {
			return insertBuilder.buildQuery(info);
		}
		
		return "";
	}
}
