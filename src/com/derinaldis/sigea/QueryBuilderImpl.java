package com.derinaldis.sigea;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public abstract class QueryBuilderImpl {
	String queryTemplate;

	QueryBuilderImpl(String resource) throws IOException {
		queryTemplate = readInputStream("/" + Defaults.resourceDir + "/" + resource);
	}

	abstract String buildQuery(QueryInfo info);

	private String readInputStream(String isName) throws IOException {
		InputStream is = getClass().getResourceAsStream(isName);
		try {
			ByteArrayOutputStream result = new ByteArrayOutputStream();
			byte[] buffer = new byte[1024];
			int length;
			while ((length = is.read(buffer)) != -1) {
				result.write(buffer, 0, length);
			}
			return result.toString("UTF-8");
		} finally {
			try {
				is.close();
			} catch (IOException e) {
			}
		}
	}

}
