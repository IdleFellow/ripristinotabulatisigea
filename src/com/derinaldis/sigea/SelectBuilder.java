package com.derinaldis.sigea;

import java.io.IOException;

import com.derinaldis.sigea.logging.MyLogger;

public class SelectBuilder extends QueryBuilderImpl {
	private static final String templateName = "query_template_select.txt";

	SelectBuilder() throws IOException {
		super(templateName);
	}

	@Override
	String buildQuery(QueryInfo info) {
		String query = queryTemplate.replace("_CTIPO_TBUT_", info.getCTIPO_TBUT())
				.replace("_CIDCV_TBUT_", info.getCIDCV_TBUT()).replace("_CCIVE_GNCA_1_", info.getCCIVE_GNCA_1_SQL())
				.replace("_CCIVE_GNCA_2_", info.getCCIVE_GNCA_2()).replace("_NPRTT_", info.getNPRTT());
		if (info.getNPRTT().length() == 0) {
			StringBuilder strB = new StringBuilder(query);
			strB.delete(query.indexOf("_START_NRPTT_BLOCK_"),
					query.indexOf("_END_NRPTT_BLOCK_") + "_END_NRPTT_BLOCK_".length());
			query = strB.toString().trim();
		} else {
			query = query.replace("_START_NRPTT_BLOCK_", "").replace("_END_NRPTT_BLOCK_", "");
		}
		MyLogger.getInstance().fine("query: " + query + ";");
		return query + ";";
	}

}
