package com.derinaldis.sigea;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class QueryInfo {
	static public enum Action {
		SELECT, INSERT, UNKNOWN
	}

	final static public List<String> COMPAGNIE = Arrays.asList(new String[] { "ITALIANA", "REALE" });

	final static String FERRE = "0";
	final static String FANLM = "N";

	final static int CTIPO_TBUT_LENGTH = 2;
	final static int CIDCV_TBUT_LENGTH = 3;
	final static int CCIVE_GNCA_2_LENGTH = 20;
	final static int NPRTT_LENGTH = 7;

	String COMPAGNIA = "";
	String CTIPO_TBUT = "";
	String CIDCV_TBUT = "";
	String CCIVE_GNCA_1 = "";
	String CCIVE_GNCA_2 = "";
	String NPRTT = "";

	Action action = Action.UNKNOWN;

	protected boolean filledIn;

	List<String> errors;

	public QueryInfo() {
	}

	public Action getAction() {
		return action;
	}

	public void setAction(Action action) {
		this.action = action;
	}


	public String getCOMPAGNIA() {
		return COMPAGNIA;
	}

	public void setCOMPAGNIA(String COMPAGNIA) {
		this.COMPAGNIA = COMPAGNIA;
	}

	public String getCTIPO_TBUT() {
		return CTIPO_TBUT;
	}

	public void setCTIPO_TBUT(String CTIPO_TBUT) {
		this.CTIPO_TBUT = CTIPO_TBUT;
	}

	public String getCIDCV_TBUT() {
		return CIDCV_TBUT;
	}

	public void setCIDCV_TBUT(String CIDCV_TBUT) {
		this.CIDCV_TBUT = CIDCV_TBUT;
	}

	public String getCCIVE_GNCA_1() {
		return CCIVE_GNCA_1;
	}

	public void setCCIVE_GNCA_1(String CCIVE_GNCA_1) {
		this.CCIVE_GNCA_1 = CCIVE_GNCA_1;
	}

	public String getCCIVE_GNCA_2() {
		return CCIVE_GNCA_2;
	}

	public void setCCIVE_GNCA_2(String CCIVE_GNCA_2) {
		this.CCIVE_GNCA_2 = CCIVE_GNCA_2;
	}

	public String getNPRTT() {
		return NPRTT;
	}

	public void setNPRTT(String NPRTT) {
		this.NPRTT = NPRTT;
	}

	public boolean isFilledIn() {
		return filledIn;
	}

	public boolean hasErrors() {
		return !(errors == null || errors.size() == 0);
	}

	public List<String> getErrors() {
		return errors;
	}

	public void checkParams() {
		errors = new ArrayList<String>();
		checkAction();
		checkCOMPAGNIA();
		checkCTIPO_TBUT();
		checkCIDCV_TBUT();
		checkCCIVE_GNCA_2();
		checkCCIVE_GNCA_1();
		checkNPRTT();
		filledIn = true;
	}

	void checkAction() {
		if (action == Action.UNKNOWN) {
			errors.add("Azione non valida");
		}
	}

	void checkCOMPAGNIA() {
		if (COMPAGNIE.contains(COMPAGNIA)) {
			return;
		}
		errors.add("Compagnia non valida");
	}

	void checkCTIPO_TBUT() {
		if (CTIPO_TBUT != null && CTIPO_TBUT.trim().length() > 0 && CTIPO_TBUT.trim().length() <= CTIPO_TBUT_LENGTH) {
			return;
		}
		errors.add("Tipo tabulato non valido");
	}

	void checkCIDCV_TBUT() {
		if (CIDCV_TBUT != null && CIDCV_TBUT.trim().length() > 0 && CIDCV_TBUT.trim().length() <= CIDCV_TBUT_LENGTH) {
			return;
		}
		errors.add("Sotto tabulato non valido");
	}

	void checkCCIVE_GNCA_2() {
		if (CCIVE_GNCA_2 != null && CCIVE_GNCA_2.trim().length() > 0
				&& CCIVE_GNCA_2.trim().length() <= CCIVE_GNCA_2_LENGTH) {
			return;
		}
		errors.add("Numero polizza non valido");
	}

	void checkCCIVE_GNCA_1() {
		if (parseCCIVE_GNCA_1() != null) {
			return;
		}
		errors.add("Data non valida");
	}

	void checkNPRTT() {
		if (NPRTT == null || (NPRTT.trim().length() <= NPRTT_LENGTH && NPRTT.matches("^\\d*$"))) {
			return;
		}
		errors.add("Numero partita non valido");
	}

	public String getCCIVE_GNCA_1_SQL() {
		Date date = parseCCIVE_GNCA_1();
		if (date == null) {
			return "";
		}
		return new SimpleDateFormat("yyyyMMdd").format(date);
	}

	private Date parseCCIVE_GNCA_1() {
		SimpleDateFormat formFormat = new SimpleDateFormat("dd-MM-yyyy");
		formFormat.setLenient(false);
		if (CCIVE_GNCA_1 != null) {
			try {
				return formFormat.parse(CCIVE_GNCA_1);
			} catch (Exception e) {
			}
		}
		return null;
	}
}
